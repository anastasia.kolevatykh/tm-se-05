package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void create(String name, String description, Date startDate, Date endDate) {
        if (name == null || name.isEmpty()) return;
        projectRepository.persist(new Project(name, description, startDate, endDate));
    }

    public List<Project> showAll() {
        List<Project> projectList = projectRepository.findAll();
        if (projectList.isEmpty()) return null;
        return projectList;
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        if (projectRepository.findOneByName(name) == null) return null;
        return projectRepository.findOneByName(name);
    }

    public void update(String id, String name, String description, Date startDate, Date endDate) {
        if (name == null || name.isEmpty()) return;
        Project project = projectRepository.findOneById(id);
        if (project != null) {
            project.setName(name);
            project.setDescription(description);
            project.setStartDate(startDate);
            project.setEndDate(endDate);
            projectRepository.merge(project);
        } else {
            projectRepository.persist(new Project(name, description, startDate, endDate));
        }
    }

    public void clearAll() {
        projectRepository.removeAll();
    }

    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(id);
    }
}
