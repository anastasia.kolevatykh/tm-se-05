package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getShortName() {
        return "tcr";
    }

    @Override
    public String getDescription() {
        return "\tCreate new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\nEnter task name: ");
        String taskName = bootstrap.getScanner().nextLine();

        System.out.println("Enter task description: ");
        String taskDescription = bootstrap.getScanner().nextLine();

        System.out.println("Enter task start date: ");
        String taskStartDate = bootstrap.getScanner().next();
        Date startDate = DateFormatter.parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        String taskEndDate = bootstrap.getScanner().next();
        Date endDate = DateFormatter.parseDate(taskEndDate);

        bootstrap.getTaskService().create(taskName, taskDescription, startDate, endDate);
        System.out.println("[OK]");
    }
}
