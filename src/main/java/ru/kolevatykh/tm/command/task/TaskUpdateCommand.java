package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getShortName() {
        return "tu";
    }

    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK UPDATE]\nEnter task name: ");
        String taskName = bootstrap.getScanner().nextLine();

        if (bootstrap.getTaskService().findOneByName(taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
        } else {
            String id = bootstrap.getTaskService().findOneByName(taskName).getId();

            System.out.println("Enter new task name: ");
            String name = bootstrap.getScanner().nextLine();

            System.out.println("Enter new task description: ");
            String description = bootstrap.getScanner().nextLine();

            System.out.println("Enter new task start date: ");
            String start = bootstrap.getScanner().nextLine();
            Date startDate = DateFormatter.parseDate(start);

            System.out.println("Enter new task end date: ");
            String end = bootstrap.getScanner().nextLine();
            Date endDate = DateFormatter.parseDate(end);

            bootstrap.getTaskService().update(id, name, description, startDate, endDate);
            System.out.println("[OK]");
        }
    }
}
