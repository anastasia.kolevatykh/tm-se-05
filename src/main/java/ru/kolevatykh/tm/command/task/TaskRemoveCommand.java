package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getShortName() {
        return "tr";
    }

    @Override
    public String getDescription() {
        return "\tRemove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\nEnter task name: ");
        String taskName = bootstrap.getScanner().nextLine();

        if (bootstrap.getTaskService().findOneByName(taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
        }

        String taskId = bootstrap.getTaskService().findOneByName(taskName).getId();

        if (taskId != null) {
            bootstrap.getTaskService().remove(taskId);
            System.out.println("[OK]");
        }
    }
}
