package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getShortName() {
        return "tcl";
    }

    @Override
    public String getDescription() {
        return "\tRemove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        bootstrap.getTaskService().clearAll();
        System.out.println("[Removed all tasks.]");
        System.out.println("[OK]");
    }
}
