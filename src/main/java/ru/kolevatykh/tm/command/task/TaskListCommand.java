package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getShortName() {
        return "tl";
    }

    @Override
    public String getDescription() {
        return "\tShow all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        List<Task> taskList = bootstrap.getTaskService().showAll();

        if (taskList == null) {
            System.out.println("No tasks yet.");
        } else {
            StringBuilder tasks = new StringBuilder();
            int i = 0;

            for (Task task : taskList) {
                tasks
                        .append(++i)
                        .append(". ")
                        .append(task.toString())
                        .append(System.lineSeparator());
            }

            String taskString = tasks.toString();
            System.out.println(taskString);
        }
    }
}
