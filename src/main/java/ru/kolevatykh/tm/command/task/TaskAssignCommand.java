package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;

public class TaskAssignCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-assign";
    }

    @Override
    public String getShortName() {
        return "ta";
    }

    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public void execute() {
        System.out.println("[ASSIGN TASK TO PROJECT]\nEnter task name:");
        String taskName = bootstrap.getScanner().nextLine();

        if (bootstrap.getTaskService().findOneByName(taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
        }

        String taskId = bootstrap.getTaskService().findOneByName(taskName).getId();

        System.out.println("Enter project name:");
        String projectName = bootstrap.getScanner().nextLine();

        if (bootstrap.getProjectService().findByName(projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
        }
        String projectId = bootstrap.getProjectService().findByName(projectName).getId();

        bootstrap.getTaskService().assignToProject(taskId, projectId);
        System.out.println("[OK]");
    }
}
