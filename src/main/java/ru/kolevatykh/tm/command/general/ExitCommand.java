package ru.kolevatykh.tm.command.general;

import ru.kolevatykh.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getShortName() {
        return "e";
    }

    @Override
    public String getDescription() {
        return "\t\tExit.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
