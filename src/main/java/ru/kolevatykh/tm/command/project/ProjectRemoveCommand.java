package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getShortName() {
        return "pr";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\nEnter project name: ");
        String name = bootstrap.getScanner().nextLine();

        if (bootstrap.getProjectService().findByName(name) == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
        } else {
            String projectId = bootstrap.getProjectService().findByName(name).getId();

            if (projectId != null) {
                bootstrap.getTaskService().removeProjectTasks(projectId);
                bootstrap.getProjectService().remove(projectId);
                System.out.println("[Removed project with tasks.]\n[OK]");
            }
        }
    }
}
