package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getShortName() {
        return "pl";
    }

    @Override
    public String getDescription() {
        return "\tShow all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        List<Project> projectList = bootstrap.getProjectService().showAll();

        if (projectList == null) {
            System.out.println("No projects yet.");
        } else {
            StringBuilder projects = new StringBuilder();
            int i = 0;

            for (Project project : projectList) {
                projects
                        .append(++i)
                        .append(". ")
                        .append(project.toString())
                        .append(System.lineSeparator());
            }

            String projectString = projects.toString();
            System.out.println(projectString);
        }
    }
}
