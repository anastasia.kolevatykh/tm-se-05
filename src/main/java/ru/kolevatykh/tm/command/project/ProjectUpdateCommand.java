package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getShortName() {
        return "pu";
    }

    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT UPDATE]\nEnter project name: ");
        String projectName = bootstrap.getScanner().nextLine();

        if (bootstrap.getProjectService().findByName(projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
        } else {
            String id = bootstrap.getProjectService().findByName(projectName).getId();

            System.out.println("Enter new project name: ");
            String name = bootstrap.getScanner().nextLine();

            System.out.println("Enter new project description: ");
            String description = bootstrap.getScanner().nextLine();

            System.out.println("Enter project start date: ");
            String start = bootstrap.getScanner().nextLine();
            Date startDate = DateFormatter.parseDate(start);

            System.out.println("Enter project end date: ");
            String end = bootstrap.getScanner().nextLine();
            Date endDate = DateFormatter.parseDate(end);

            bootstrap.getProjectService().update(id, name, description, startDate, endDate);
            System.out.println("[OK]");
        }
    }
}
