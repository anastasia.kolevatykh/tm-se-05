package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getShortName() {
        return "pcr";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        String projectName = bootstrap.getScanner().nextLine();

        System.out.println("Enter project description: ");
        String projectDescription = bootstrap.getScanner().nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = bootstrap.getScanner().next();
        Date startDate = DateFormatter.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = bootstrap.getScanner().next();
        Date endDate = DateFormatter.parseDate(projectEndDate);

        bootstrap.getProjectService().create(projectName, projectDescription, startDate, endDate);
        System.out.println("[OK]");
    }
}
