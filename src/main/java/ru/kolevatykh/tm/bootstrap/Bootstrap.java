package ru.kolevatykh.tm.bootstrap;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.repository.ProjectRepository;
import ru.kolevatykh.tm.repository.TaskRepository;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;

import java.util.*;

public class Bootstrap {
    private Scanner scanner = new Scanner(System.in);
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Scanner getScanner() {
        return scanner;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void registry(final AbstractCommand command) throws Exception {
        final String commandName = command.getName();
        final String commandShortName = command.getShortName();
        final String commandDescription = command.getDescription();

        if (commandName == null || commandName.isEmpty()) {
            throw new Exception("There's no such command name.");
        }
        if (commandDescription == null || commandDescription.isEmpty()) {
            throw new Exception("There's no such command description.");
        }
        command.setBootstrap(this);
        commands.put(commandName, command);

        if (commandShortName != null && !commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() throws Exception {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            return;
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            return;
        }
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init(AbstractCommand... commandInstances) {
        try {
            for (AbstractCommand command : commandInstances) {
                registry(command);
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
