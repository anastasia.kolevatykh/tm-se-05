package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap;

    public ProjectRepository() {
        this.projectMap = new HashMap<>();
    }

    public List<Project> findAll() {
        List<Project> projectList = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            projectList.add(entry.getValue());
        }
        return projectList;
    }

    public Project findOneById(String id) {
        return projectMap.get(id);
    }

    public Project findOneByName(String name) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void merge(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void remove(String id) {
        for (Iterator<Map.Entry<String, Project>> it = projectMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> entry = it.next();
            if (entry.getKey().equals(id)) {
                it.remove();
            }
        }
    }

    public void removeAll() {
        projectMap = new HashMap<>();
    }
}
