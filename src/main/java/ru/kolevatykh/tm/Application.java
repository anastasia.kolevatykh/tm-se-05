package ru.kolevatykh.tm;

import ru.kolevatykh.tm.bootstrap.Bootstrap;
import ru.kolevatykh.tm.command.*;
import ru.kolevatykh.tm.command.general.*;
import ru.kolevatykh.tm.command.project.*;
import ru.kolevatykh.tm.command.task.*;

public class Application {
    private static AbstractCommand[] commandInstances = {
            new HelpCommand(),
            new ProjectCreateCommand(),
            new ProjectListCommand(),
            new ProjectShowCommand(),
            new ProjectUpdateCommand(),
            new ProjectRemoveCommand(),
            new ProjectClearCommand(),
            new TaskCreateCommand(),
            new TaskListCommand(),
            new TaskAssignCommand(),
            new TaskUpdateCommand(),
            new TaskRemoveCommand(),
            new TaskClearCommand(),
            new ExitCommand()
    };

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commandInstances);
    }
}
